import java.util.ArrayList;

public class Employees {
    String name;
    String surname;

    public Employees(String name, String surname){
        this.name = name;
        this.surname = surname;
    }

    String generateEmail(){
        return this.name + '.' + this.surname + "@mex.com";
    }

}
