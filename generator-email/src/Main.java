import java.util.Scanner;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        boolean finishProgram = true;
        ArrayList <String> employeesList = new ArrayList<>();

        while(finishProgram){
            Scanner input = new Scanner(System.in);

            System.out.print("Your first name: ");
            String fname = input.next();
            System.out.print("Your last name: ");
            String lname = input.next();

            Employees employees = new Employees(fname, lname);

            System.out.println("Your email:" + employees.generateEmail());

            employeesList.add(employees.generateEmail());

            System.out.print("Do you want add another employee? yes or no: ");
            String decisionFinishProgram = input.next();

            if("no".equals(decisionFinishProgram)){
                finishProgram = false;
            }
        }

        System.out.print("Employees list: ");
        for (String x : employeesList)
        {
            System.out.println(x);
        }
    }
}
