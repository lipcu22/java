package com.company;

public class Main {

    public static void main(String[] args) {

        //Exercise 1
        int a = 1;
        int b = 10;

        if(a == 10 || b == 10){
            System.out.printf("teen");
        }

        //Exercise 2
        int c = 5;
        int d = 6;
        int e = 13;
        int sum = 0;

        if(c == 13){
            sum = 0;
        }else if(d == 13){
            sum += c;
        }else if(e == 13){
            sum = c + d;
        }else{
            sum = c + d + e;
        }

        System.out.printf("Sum:" + sum);

        //Exercise 3
        int[] array = {3,2,14,1,2,3,6};

        for (int i = 0; i < array.length; i++) {
            if(array[i] == 1 && array[i+1] == 2 && array[i+2] == 3){
                System.out.printf("true");
            }
        }
    }
}
